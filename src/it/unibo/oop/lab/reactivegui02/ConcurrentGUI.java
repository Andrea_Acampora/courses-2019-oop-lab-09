package it.unibo.oop.lab.reactivegui02;
import java.awt.Dimension;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


public class ConcurrentGUI extends JFrame {


	private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
	private final JLabel number = new JLabel("0");
	private final JButton upButton = new JButton("up");
	private final JButton downButton = new JButton("down");
	private final JButton stopButton = new JButton("stop");


	public ConcurrentGUI() {

	  super();
	  final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	  this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
	  this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	  final JPanel panel = new JPanel();	
	  panel.add(number);
	  panel.add(upButton);
	  panel.add(downButton);
	  panel.add(stopButton);
	  this.getContentPane().add(panel);
	  this.setVisible(true);

      final Agent agent = new Agent();
      new Thread(agent).start();

      stopButton.addActionListener(new ActionListener() {

		public void actionPerformed(final ActionEvent e) {
			agent.stopCounting();
			upButton.setEnabled(false);
			downButton.setEnabled(false);

		}
	});
      upButton.addActionListener(new ActionListener() {

		public void actionPerformed(final ActionEvent e) {
		agent.increment();
		}
	});

      downButton.addActionListener(new ActionListener() {

		public void actionPerformed(final ActionEvent e) {
		agent.decrement();
		}
	});


	}
	
	
	private class Agent implements Runnable{

		private volatile boolean flag = false;
		private volatile boolean stop = false;
		private volatile boolean startFlag = false;

		private volatile int counter;

		public void decrement() {
		startFlag = true;
		this.flag = false;
		}


		public void increment() {
			startFlag = true;
			this.flag = true;
		}

		public void run() {
			while (!this.stop) {
				try {
					SwingUtilities.invokeAndWait(new Runnable() {
                        @Override
                        public void run() {
                            ConcurrentGUI.this.number.setText(Integer.toString(Agent.this.counter));
                        }
                    });
					if (startFlag) {
                    if (this.flag) {
                    	this.counter++;
                    } else {
                    	this.counter--;
                    }
                    Thread.sleep(100);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}

		public void stopCounting() {
            this.stop = true;
        }
	}
}
